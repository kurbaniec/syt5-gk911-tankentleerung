## Kacper Urbaniec | 5AHIT | 02.10.2019  

#  "*Entleerung eines Tanks*"

##  Ziele

Du kannst einfache Steuerungsaufgaben in einer Industrieprogrammiersprache nach ISO 61131-3 umsetzen. Du kannst die Steuerungsprogramme simulieren und auf Fehler testen (debuggen). Der Zusammenhang von Geräten, Tasks, Adressen, Variablen ist bekannt. Du kannst eine einfache Visualisierung für eine Steuerung erstellen.

## Aufgaben

1. Erstelle in CodeSys 3.5 ein neues „Standardprojekt“ mit einem Gerät „Codesys WinControl V3“

2. Implementiere mittels Funktionsblöcken (FUP) eine Steuerung für folgende Anlagenbeschreibung. Achte auf sinnvolle Variablenbezeichnungen (siehe auch Abschnitt "Ergebnisse").    

   *Der Tank einer Mischanlage soll über drei an verschiedenen Orten angebrachten Schaltern S1, S2, S3 entleert werden können. Das Auslassventil V4 zum Entleeren soll dabei von jedem Schalter ein und ausgeschaltet werden können (Funktionsweise wie bei einer Kreuzschaltung in der Elektrik, Kreuzschaltung = Wechselschaltung mit drei Schaltern).*  

   Das Programm soll in einen zyklischen Task mit einer Zykluszeit von 200ms eingebettet werden. Führe das Programm auf der SoftSPS Win Control V3 aus.

3. Erweitere das Projekt mittels einer zweiten POU mit Strukturiertem Text (ST) um folgende Funktion:  

   *Zwecks Überlaufschutz wird ein digitaler Sensor S5 im Ruhestromprinzip verbaut, dessen Signal bei einer bestimmte Füllhöhe „Level_Full“ des Tanks abfällt (d.h. auf 0 fällt = negative Logik). Dann muss das Auslassventil V4 umgehend geöffnet werden. Ist der Flüssigkeitspegel unter den Füllstand „Level_Full“ gefallen, so liefert das Signal S5 wieder 1 und das Auslassventil soll geschlossen werden.*     

   Vermeide dabei mögliche Kollisionen mit der Funktion aus Aufgabe 2.

4. Erstelle einen Funktionsbaustein „Kreuzschaltung“ mit drei Eingängen und einem Ausgang, der die Funktion der Kreuzschaltung aus Aufgabe 2 kapselt. Teste den Funktionsbaustein, indem Aufgabe 1 in einem neuen Programm damit umgesetzt wird (der ursprüngliche Code von Aufgabe 2 muss erhalten bleiben, also wirklich eine neue POU erstellen!)

5. Erstelle eine funktionierende Visualisierung mit folgenden Eigenschaften:

   * Die Schalter S1,S2,S3 können bedient werden
   * Der Zustand der Signale V4 und S5 wird über Lampen dargestellt
   * Der Wert von „Level_Full“ kann über ein numerisches Eingabefeld festgelegt werden.
   * Um einen Signalwechsel von S5 auslösen zu können, soll über einen vertikalen Slider ein fiktiver Tank-Füllstand eingestellt werden können. Der eingegebene Wert von „Level_Full“ muss dabei sowohl über- als auch unterschritten werden können.

6. Teste die Visualisierung und das Zusammenspiel mit deinem Programmen mit auf der SoftSPS.

## Implementierung

### Handskizze

Die Handskizze zeigt die Aufgabenstellung mittels R&I-Fließschema auf.  Bei der Umsetzung habe ich mich vor allem auf diesen [Wikipedia-Eintrag](https://de.wikipedia.org/wiki/R%26I-Fließschema) und diese [Symbolsammlung](https://www.edrawsoft.com/de/pidsymbols.php) angelehnt. 

Die Aufgabe umfasst grob sechs Komponenten die auf der Skizze dargestellt werden:

* Tank 
  * Flüssigkeitsbehälter, der durch ein Ventil entleert werden kann 
  * Benennung B0001 
* Auslassventil 
  * Dargestellt mittels elektrischen/magnetischen Ventil, da die drei Schalter bzw. der Überlauf-Sensor per Impuls das Ventil öffnen sollen können
  * Benennung YS (Steller, Schaltfunktion)
*  3x Schalter
  * Mittels Instrumenten (runde Kreise) dargestellt, elektrische Verbindungen sind strichliert eingetragen
  * Benennung  HS (Handeingabe, Schaltfunktion) + Nummer
* Überlauf-Sensor
  * Mittels Instrument dargestellt
  * Benennung LZ (Füllstand, sicherheitsrelevante Schaltfunktion)

![](img/skizzeDC.PNG)

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

### Umsetzung mit Codesys

Zuerst musste eine Funktion überlegt werden die, die die folgende Wahrheitstabelle erfüllt. Die Tabelle entspricht dabei der Aufgabenstellung.

| HS1  | HS2  | HS3  | YS   |
| ---- | ---- | ---- | ---- |
| 0    | 0    | 0    | 0    |
| 0    | 0    | 1    | 1    |
| 0    | 1    | 0    | 1    |
| 0    | 1    | 1    | 0    |
| 1    | 0    | 0    | 1    |
| 1    | 0    | 1    | 0    |
| 1    | 1    | 0    | 0    |
| 1    | 1    | 1    | 1    |

HS1-3 steht für die Zustände der Schalter und YS für das Ergebnis, das den Ventilzustand beschreibt (0 - Ventil geschlossen, 1 - Ventil offen).

Nach längeren überlegen und ein bisschen Trial-And-Error kam ich auf eine Funktion, die den Ventilzustand regelt:   $({HS_1} \underline{\vee} HS_2) \underline{\vee} HS_3 $

---

Da die Funktion jetzt bekannt war, konnte ich diese einfach in Codesys mittels zwei `XOR`-Blöcken nachbauen:

![](img/2.PNG)

Die Benennung ist immer noch die selbe wie in der Handskizze, `HS1-3` steht für die Schalter, `YS` steht für das Regelventil.

Jetzt konnte das Programm getestet werden, dazu muss im Hintergrund vorher `CODESYS Control Win V3` gestartet werden. 

Das Programm selbst startet man mit `Übersetzen` - `Einloggen` (Mit Download einloggen) - `Start`.

Zum Testen kann man jetzt bei der Spalte `Vorbereiteter Wert` eines Programmes einen Wert oder mehrere Werte setzten, die dann mit `Debug` - `Werte schreiben` ins Programm geschrieben werden.

Ich bin mehrere Schalterkombination durchgegangen und habe geschaut ob sie mit der Wahrheitstabelle zusammenpassen. Wie erwartet taten sie es.

![](img/2_run.PNG)

---

Für die Aufgabe 3 musste ich das bisherige Programm leicht umändern. Zuerst habe ich eine globale Variablenliste `GVL` (`Applikation` - `Objekt hinzufügen` - `Globale Variablenliste`) erstellt. In diese habe ich dann die Variable `LZ`für den Überlaufschutz definiert, um auf diese später im "Hauptprogramm" zugreifen zu können.

![](img/gvl.PNG)

Als nächstes habe ich den Überlaufschutz mit dem Programm `POU_Task_3` definiert. Dieser macht nichts anderes als den Sensor `LZ` auf 0 zu stellen (negative Logik), wenn eine gewisse Füllhöhe erreicht worden ist. Da `LZ` eine globale Variable ist, muss vorher der Name der globalen Variablenliste angegeben werden, in meinem Fall `GVL.LZ`.

![](img/3.PNG)

Jetzt muss der Überlaufschutz ins Hauptprogramm `PLC_PRG` eingebettet werden. Dazu habe einfach einen `ODER`-Block hinzugefügt, bei dem ein Eingang das Resultat der Schalter übernimmt und der andere Eingang den derzeitigen Stand des Überlaufschutzes mittels `GVL.LZ` einliest. Somit kann der Überlaufschutz das Ventil immer öffnen, unabhängig von den Schaltern. Achtung: Beim Eingang für den Überlaufschutz muss eine Negation eingetragen werden, da der Überlaufschutz den Tank öffnen soll, wenn ihr Wert auf 0 fällt. Außerdem muss bei `Taskkonfiguration - MainTask` das neue Programm hinzugefügt werden, damit dieses mit-gestartet wird.

![](img/3.PNG)

Jetzt konnte ich den Überlaufschutz ausprobieren, indem ich in `POU_Task_3` die `LEVEL_Current`-Variable beeinflusst habe:

Überlaufschutz an:

![](img/3_run_on.PNG)

Überlaufschutz aus:

![](img/3_run_off.PNG)

---

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

Um einen neuen Funktionsbaustein zu definieren erzeugt man einfach eine neue `POU`, doch anstatt Programm wählt man Funktionsbaustein aus.

![](img/kreuz.PNG)

Die Kreuzschaltung selbst wird wie im ersten Programm`PLC_PRG`einfach aus zwei`XOR`-Blöcken gebildet. Jetzt hat man aber nicht einen `VAR`-Block oben, sondern zwei getrennte Blöcke - `VAR_INPUT` für die Eingänge und `VAR_OUTPUT` für die Ausgänge. 

![](img/kreuzschaltung.PNG)

Jetzt habe ich ein neues Programm namens `POU_Task_4` erstellt, dass wie das erste Programm `PLC_PRG` arbeiten soll, aber die Kreuzschaltung für die Schalter benutzen soll.

Um die Kreuzschaltung im Programm zu verwenden geht man auf das rechte Fenster `Werkzeuge`, geht zum Reiter `Bausteine` und zieht das erstellte Programm `Kreuzschaltung` ins aktuelle Programm hinein.

Jetzt erstellt man wieder einen `OR`-Block für den Überlaufschutz und hat eine vereinfachte Version von ursprünglichen Programm:

![](img/4.PNG)

Das Programm kann wieder getestet werden, dazu muss allerdings zuerst das ursprüngliche Programm `PLC_PRG` aus der Taskkonfiguration entfernt werden und durch `POU_Task_4` ersetzt werden.

Man sieht, dass das Programm genau gleich wie vorher arbeitet, nur dass man jetzt die Kreuzschaltung vereinfacht hat:

![](img/4_run.PNG)

---

Als Grundlage für die Visualisierung benutze ich  die Programme `POU_Task_4` und `POU_Task_3`.

Der Tank wird durch eine Balkenanzeige repräsentiert, wo ein blauer Balken den Flüssigkeitspegel symbolisiert. Der derzeitige Pegel kann über ein Potentiometer mit der Aufschrift `LEVEL CURRENT` beeinflusst werden.  Der maximale Wert kann über ein Eingabefeld `LEVEL FULL` geregelt werden. Daneben sind die Schalter platziert, die einzeln betätigt werden können.

Der Zustand des Ventils wird über eine Lampe `YS` dargestellt, wobei leuchten besagt, dass das Ventil offen ist.

Der Zustand des Überlaufschutzes wird  über eine Lampe `LZ` dargestellt. Leuchtet sie, heißt das, dass alles im grünen Bereich ist. Leuchtet sie nicht, heißt das, dass der Überlaufschutz das Ventil öffnen muss.

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

Nachfolgend sind mehre Zustände dargestellt:

*Grundzustand, keine Schalter betätigt, Überlaufschutz greift nicht ein:*

![](img/v_normal.PNG)

*Pegel über maximalen Pegel, Überlaufschutz greift ein:*

![](img/v_uberlauf.PNG)

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

*Ein Schalter betätigt, dadurch geöffnetes Ventil:*

![](img/v_schalter.PNG)





## Quellen

* [Codesys - Globale Variablen; 02.10.2019](https://help.codesys.com/api-content/2/codesys/3.5.12.0/en/_cds_vartypes_var_global/)
* [Codesys - IF-ELSE Block; 02.10.2019](https://help.codesys.com/api-content/2/codesys/3.5.12.0/en/_cds_st_instruction_if/)
* [Codesys - Create own function block; 02.10.2019](https://www.youtube.com/watch?v=SLQEtTvi3m8)
* [Codesys - Ausdrücke; 02.10.2019](https://infosys.beckhoff.com/english.php?content=../content/1033/tcplccontrol/html/TcPlcCtrl_Languages%20ST.htm&id=)
* [Codesys - Tankvisualisierung; 04.10.2019](https://www.youtube.com/watch?v=f1ccylMZUP4)
* [Kennbuchstaben; 02.10.2019](https://de.wikipedia.org/wiki/R%26I-Flie%C3%9Fschema#Erstbuchstabe_%E2%80%93_PCE-Kategorie)
* [Kennsymbole; 04.10.2019](https://www.edrawsoft.com/de/pidsymbols.php)